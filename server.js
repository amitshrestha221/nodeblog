const express = require('express')
const bodyParser = require('body-parser')

var routes = require('./routes').route

const app = express()
app.use(bodyParser.urlencoded({ extended: false}))
app.use(bodyParser.json())

app.get('/test', function(req,res){
    res.send('Welcome to Blog by Amit');
});

app.listen(3000, function() {
    console.log('listening on 3000');
})
