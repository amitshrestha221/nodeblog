const app = require('express');
const router = require('router');

var addRoute = require('./add').route;

router.use('/add').addRoute;

module.exports.route = addRoute;