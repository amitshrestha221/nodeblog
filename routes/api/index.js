const express = require('express');
const router = require('router');

var blogRoutes = require('./blog').route;

router.use('/blog').blogRoutes;

module.exports.route = router;