const express = require('express');
const router = express.Router();
var apiRoute = require('./api').route;

route.use('/api', apiRoute);

module.exports.route = router;